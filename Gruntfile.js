module.exports = function(grunt) {

  require('load-grunt-tasks')(grunt);
  require('time-grunt')(grunt);



  var fconfig = function(expand, cwd, src, dest, ext) {
    var c = {
      expand: expand,
      cwd: cwd,
      src: src,
      dest: dest,
      ext: ext
    };
    return c;
  };

  var config = function(src, dest) {
    var c = {
      src: src,
      dest: dest
    };
    return c;
  };

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    jshint: {
      options: {
        reporter: require('jshint-stylish'),
        reporterOutput: "",
        globals: {
          'jQuery': true,
          'angular': true,
          'console': true,
          '$': true,
          '_': true,
          'moment': true
        }
      },
      app: {
        src: ['Gruntfile.js', 'src/**/**.js']
      }
    },

    htmlhint: {
      options: {
        'attr-lower-case': true,
        'attr-value-not-empty': false,
        'tag-pair': true,
        'tag-self-close': true,
        'tagname-lowercase': true,
        'id-class-value': true,
        'id-unique': true,
        'img-alt-require': true,
        'img-alt-not-empty': true
      },
      app: {
        src: ['src/**/**.html']
      }
    },
    copy: {
      fonts: {
        expand: true,
        files: []
      },
      html: {
        expand: true,
        files: [
          {expand: true, cwd: 'src', src:"**/**.html", dest:'build'}
        ]
      }
    },

    watch: {
      options: {
        nospawn: true,
        keepalive: true,
        livereload: true
      },
      app_grunt: {
        files: ['Gruntfile.js'],
        tasks: ['build']
      },
      app_html: {
        files: ['src/**/**.html'],
        tasks: ['htmlhint:app', 'copy:html','injector:app']
      },
      app_js: {
        files: ['src/**/**.js>'],
        tasks: ['jshint', 'injector:app']
      },
      app_css: {
        files: ['src/**/**.css', 'injector:app'],
        tasks: []
      }
    },

    injector: {
      options: {
        relative:true
      },
      app: {
        files: {
          'build/index.html': [
          "./bower_components/angular-material/angular-material.min.css",
          'src/**/**.css',
          "./bower_components/angular/angular.min.js",
          "./bower_components/angular-resource/angular-resource.min.js",
          "./bower_components/angular-ui-router/release/angular-ui-router.min.js",
          "./bower_components/angular-animate/angular-animate.min.js",
          "./bower_components/angular-messages/angular-messages.min.js",
          "./bower_components/angular-material/angular-material.min.js",
           'src/**/**.js'],
        }
      }
    },

    connect: {
      server: {
        options: {
          port: 8080,
          base: '',
          hostname: '*', //so wierd
          livereload: true,
          middleware: function(connect, options, middlewares) {
            middlewares.unshift(require('grunt-connect-proxy/lib/utils').proxyRequest);
            return middlewares;
          }
        },
        proxies: [{
          context: '/api',
          host: '127.0.0.1',
          port: 80,
          changeOrigin: true,
          rewrite: {
            '^/api': '/'
          }
        }]
      }
    },



  });

  grunt.registerTask('build', function (target) {
       grunt.task.run([
           'copy',
           'htmlhint',
           'jshint',
           'injector'
       ]);
   });

  grunt.registerTask('serve', function (target) {
       grunt.task.run([
           'build',
           'configureProxies:server',
           'connect',
           'watch'
       ]);
   });

};
