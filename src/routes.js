(function() {

  'use strict';
  var app = angular.module('MainRoutes', []);
  app.config(function($stateProvider) {

    $stateProvider
      .state('app', {
        abstract: true,
        url: '',
        //controller: 'AppController',
        templateUrl: './pages/app/app.html'
      })
      .state('app.home', {
        url: '/home',
        templateUrl: './pages/home/view.html',
      })
      .state('app.other', {
        url: '/other',
        templateUrl: './pages/other/view.html',
      });

  });

})();
