(function() {

	'use strict';

	var app = angular.module('app', [
		'ui.router',
		'MainRoutes'
	]);

	app.config(function($urlRouterProvider) {


        $urlRouterProvider.otherwise('/home');

	}).run(function() {
      //run configuration stuff here
	});

})();
